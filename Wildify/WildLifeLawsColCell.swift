//
//  WildLifeLawsColCell.swift
//  Wildify
//
//  Created by Arslan Siddique on 21/01/2021.
//  Copyright © 2021 Thor Remiendo. All rights reserved.
//

import UIKit

class WildLifeLawsColCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.shadowView()
    }
}

